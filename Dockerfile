FROM node:16.20.1-alpine AS base

WORKDIR /app/

COPY package*.json ./

RUN npm install --include=dev\
    npm run build 
  
COPY . .

FROM node:16.20.1-alpine

WORKDIR /app/

COPY --from=base /app/ ./

CMD ["npm", "start"]


